// @flow

import * as React from 'react'
import { Icon, List, Checkbox, Input } from 'antd'
import { ITodo } from '../../types'

type Props = {
  item: ITodo,
  removeTodoItem: (id: string) => void,
  updateTodoItem: (id: string, todo: ITodo) => void,
}
type State = {
  editing: boolean,
  editValue: string,
}

export class TodoListItem extends React.Component<Props, State> {
  static initialState = {
    editing: false,
    editValue: '',
  }

  state = TodoListItem.initialState

  constructor(props: Props) {
    super(props)
    this.editInputRef = React.createRef()
  }

  handlePressEnterEditTodo = () => {
    const newCurrentEditedTodo = {
      ...this.props.item,
      title: this.state.editValue,
    }
    this.props.updateTodoItem(this.props.item.id, newCurrentEditedTodo)
    this.setState(TodoListItem.initialState)
  }

  updateStateInputValue = (e: SyntheticEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
    })
  }

  toggleTodoItem = (todo: ITodo) => {
    this.props.updateTodoItem(todo.id, { ...todo, done: !todo.done })
  }

  removeTodoItem = (todo: ITodo) => {
    this.props.removeTodoItem(todo.id)
  }

  toggleEditing = () => {
    this.setState(
      state => ({
        editing: !state.editing,
        editValue: !state.editing ? this.props.item.title : '',
      }),
      () => {
        if (this.state.editing) {
          this.editInputRef.current.input.focus()
        }
      },
    )
  }

  handleKeyDown = e => {
    // on Escape
    if (e.keyCode === 27) {
      this.setState(TodoListItem.initialState)
    }
  }

  editInputRef: React.Ref<any>

  render() {
    const { item } = this.props
    return this.state.editing ? (
      <List.Item data-testid="list-item">
        <Input
          aria-label="Edit Value"
          name="editValue"
          ref={this.editInputRef}
          value={this.state.editValue}
          onChange={this.updateStateInputValue}
          onKeyDown={this.handleKeyDown}
          onPressEnter={this.handlePressEnterEditTodo}
        />
      </List.Item>
    ) : (
      <List.Item
        data-testid="list-item"
        actions={[
          <Icon
            key={`${item.id}-edit-todo`}
            data-testid="edit-todo"
            type="edit"
            theme="filled"
            onClick={this.toggleEditing}
          />,
          <Icon
            key={`${item.id}-remove-todo`}
            data-testid="remove-todo"
            type="close-circle"
            theme="filled"
            onClick={() => this.removeTodoItem(item)}
          />,
        ]}
      >
        <Checkbox
          data-testid="checkbox"
          value={item.done}
          checked={item.done}
          onChange={() => this.toggleTodoItem(item)}
        >
          <span data-testid="title">{item.title}</span>
        </Checkbox>
      </List.Item>
    )
  }
}

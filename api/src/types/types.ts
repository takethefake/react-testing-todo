import { Request } from 'express';
import { Connection } from 'typeorm';
import { User } from '../entity';

export interface IObject {
    [key: string]: any;
}

export interface IContext {
    connection: Connection;
    user?: User;
    req?: Request;
}

export type mapperFn = (entity: any, field: string, value: any) => any;

export interface ILogin {
    email: string;
    password: string;
}

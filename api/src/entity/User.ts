import { Entity, Column, PrimaryGeneratedColumn, BaseEntity, OneToMany } from 'typeorm';
import { Todo } from './Todo';

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column({ type: 'varchar', length: 100 })
    firstName: string;

    @Column({ type: 'varchar', length: 100 })
    lastName: string;

    @Column({ type: 'varchar', length: 100 })
    email: string;

    @Column({ type: 'varchar', length: 100, nullable: true })
    password: string;

    @OneToMany(() => Todo, p => p.creator, {
        cascade: ['insert', 'remove'],
    })
    todos: Promise<Todo[]>;

    getTodos() {
        if (!Array.isArray(this.todos)) {
            this.todos = Promise.resolve([]);
        }
        return this.todos;
    }

    async addTodos(todosInput: Todo[]) {
        if (!Array.isArray(this.todos)) {
            this.todos = Promise.resolve([]);
        }

        const todos = todosInput.map(t => {
            const created = Todo.create(t);
            created.creator = this;
            return created;
        });

        const loadedRelation = await this.todos;
        loadedRelation.push(...todos);
        this.todos = Promise.resolve(loadedRelation);
    }

    async removeTodos(todos: Todo[]) {
        this.todos = Promise.resolve(
            ((await this.todos) || []).filter(a => !!todos.find(b => a.id === b.id)),
        );
    }

    toJSON() {
        const { firstName, lastName, email } = this;

        return {
            firstName,
            lastName,
            email,
        };
    }
}

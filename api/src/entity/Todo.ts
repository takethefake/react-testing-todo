import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    BaseEntity,
    ManyToOne,
    CreateDateColumn,
} from 'typeorm';
import { User } from './User';

@Entity()
export class Todo extends BaseEntity {
    @PrimaryGeneratedColumn('uuid') id: string;

    @Column({ type: 'text', nullable: true })
    title?: string;

    @Column({ type: 'boolean', default: false })
    done: boolean;

    @CreateDateColumn() createdAt: string;

    @ManyToOne(type => User, u => u.todos, { cascade: ['insert'] })
    creator: User;
}
